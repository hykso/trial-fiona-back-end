# FightCamp API Trial

Welcome to the FightCamp's Backend Trial. Today is meant to test your skills with Node.js, Javascript, Express.js, relational databases, and developing an elegant, sophisticated API. It's an opportunity for us to know you in your working environment and how you manage your time. Above all, it's also a good opportunity for you to see the backstage of FightCamp and how awesome we are 😎
 
## Description

The goal of this project will be to help our Content Team manage their Daily `Schedule` for filming `Workouts` in the FightCamp Studio. You will focus on building the API that will enable this tool. 

A `Schedule` represents a day of filming where the Content team will shoot around 4 to 10 `Workouts`.  Every week the producers create schedules by communicating with the `Trainers` to know their availability. Once a schedule is set with all the workouts, its status can be changed to `ready.`

## Requirements

#### For this project, you need to use 

- Node 10 or higher
- Express.js
- Sqlite3
- Jest

#### API

`Schedules` should have at least the following

- `Filming Date` - the day content will be filmed,  `MM-DD-YYYY`
- `Status` - state of the schedule like `draft`, `ready` and `canceled`

`Workouts` should have at least the following

- `Schedule` - the Workout is a part of
- `Filming Time` - time the workout will be filmed `hh:mm`
- `Status` - workouts' availability like `available`, `full` and `canceled`
- `Trainers` - person who will be doing this workout, there can be more than one Trainer
- `Level` - difficulty like `open` or `intermediate`

`Trainers` should have at least the following

- `Name`
- `Picture`

The completed API should be able to do the following:

- Get all the schedules ordered by date (ascending or descending)
- Get all schedules filtered by date or by status
- Create a new schedule
- Get a specific schedule
- Update a specific schedule
- Get all the workouts in a specific shedule
- Get all workouts in a specific schedule filtered by trainers, by level or by status
- Get a specific workout
- Update a specific workout

* Your GET endpoints should ideally be built in a way that supports pagination.

### Additional Requirements

For the purposes of having a fully functioning prototype, we would like to see
Middleware built into your server for `Auth` (we prefer you avoid using plugins like Passport)
and `Error handling`.

### Bonus

Add caching

## Demo at the end of the day, you will:

- Using Postman or any alternatives, test each endpoint
- Present the architecture of the database
- Present your code and how it is structured
- Finally, we will ask some technical question about the project

## Other Directives

No database libraries.
    
## What will be evaluated

- Good habits in project's structure
- Clean code
- Scalable architecture/solution
- Proper use of Express middlewares

## Quick tips

Don't be afraid to ask questions if you are not sure with certains aspects of the trial.
Make sure to breakdown the projects into smaller parts. It's okay if the project is not completed at the end of the trial, we don't expect it to be with the short timeframe, but being able to demo working features will help during the evaluation.

## Project submission

Use Git to commit your code during your trial and push your solution to the this repository.

